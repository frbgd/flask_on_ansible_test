FROM python:3.8-alpine

RUN apk add build-base libffi-dev openssl-dev

COPY requirements.txt /
RUN pip3 install -r /requirements.txt

COPY app /app
WORKDIR /app

CMD flower -A celery_app --port=5555
