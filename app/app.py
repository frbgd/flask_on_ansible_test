from flask import Flask

from settings import TEST_ENV
from celery_app import simple_task, long_task

app = Flask(__name__)


@app.route('/')
def root():
    return str(TEST_ENV)


@app.route('/celery/')
def celery():
    simple_task.delay()
    return 'simple_task3 started'


@app.route('/celery/long/')
def celery_long():
    long_task.delay()
    return 'long_task started'


if __name__ == '__main__':
    app.run('0.0.0.0')
