import time

from celery import Celery

from settings import BROKER_URL

app = Celery('app', broker=BROKER_URL)


@app.task
def simple_task():
    return True


@app.task
def long_task():
    time.sleep(30)
    return 'yes'
