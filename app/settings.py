import os

TEST_ENV = os.getenv('TEST_ENV', 'sometest')
BROKER_URL = os.getenv('BROKER_URL')
